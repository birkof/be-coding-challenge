<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220613201430 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE analytics_log (id INT AUTO_INCREMENT NOT NULL, line BIGINT NOT NULL, status_code INT NOT NULL, logging_date DATETIME NOT NULL, service_name VARCHAR(128) NOT NULL, request VARCHAR(255) DEFAULT NULL, INDEX IDX_FEC5FD539060CAEA (service_name), INDEX IDX_FEC5FD53AEDD75CA (logging_date), INDEX IDX_FEC5FD534F139D0C (status_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE analytics_log');
    }
}
