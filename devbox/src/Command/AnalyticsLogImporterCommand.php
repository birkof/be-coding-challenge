<?php

namespace App\Command;

use App\Entity\AnalyticsLog;
use App\Repository\LogRepository;
use DateTime;
use Doctrine\DBAL\ConnectionException;
use Exception;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception as DbalException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use LimitIterator;
use RuntimeException;
use SplFileObject;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:import-analytics-logs',
    description: 'Import logs from analytics file into database for further processing.',
    aliases: ['app:import-analytics', 'app:import-logs', 'app:analytics-log-importer', 'app:analytics-log-import'],
    hidden: false
)]
class AnalyticsLogImporterCommand extends Command
{
    use LockableTrait;

    private const BATCH_SIZE = 50;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LogRepository          $logRepository,
        string                                  $name = null,
    )
    {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('filename', InputArgument::REQUIRED, 'Specify filename to import.')
            ->addOption('reset', null, InputOption::VALUE_NONE, 'Specify if should reset previous imports.');
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if (!$this->lock()) {
            $io->error('Importer command is already running in another process.');

            return Command::SUCCESS;
        }

        $io->comment('Processing Log Analytics file...');

        // Checks if the file really exists and accessible.
        if (!$contents = file($input->getArgument('filename'), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)) {
            throw new RuntimeException('Provided filename could not be found.');
        }

        // Cache total rows available on import file.
        $totalEntriesAvailable = count($contents);

        // Reset table for clean new entries
        if ($input->getOption('reset')) {
            $this->cleanAllAnalyticsEntries();
        }

        // Turn off logging
        $connection = $this->entityManager->getConnection();
        $connection->getConfiguration()->setSQLLogger(null);

        // There are a lot of rows to import, so we import them in chunks to use less RAM.
        $batchSize = self::BATCH_SIZE;

        // Mimic an OFFSET by store the last imported entry id and
        // to select rows above that ID instead of using an actual OFFSET.
        $offsetLine = 0;

        // Set a title for this process
        $io->title('Start importing Log Analytics entries...');

        // Skip entire process if there are no entries available.
        if (!$totalEntriesAvailable) {
            $io->note('No entries to process!');

            return Command::SUCCESS;
        }

        // Start importing entries from where it left off when interrupted.
        if (
            !$input->getOption('reset')
            && $lastLogEntry = $this->logRepository->findLastInsertedRow()
        ) {
            $offsetLine = $lastLogEntry->getLine() + 1;
        }

        $io->note(sprintf('We have a total of %d lines to process and import starts from line %d.', $totalEntriesAvailable, $offsetLine));

        // Instantiate the ProgressBar
        $progressBar = new ProgressBar($output, $totalEntriesAvailable - $offsetLine);

        // Make ProgressBar look cool
        $progressBar->setFormat(
            "<fg=white;bg=blue;options=bold> %status:-45s%</>\n%current%/%max% [%bar%] %percent:3s%%\n🏁  %estimated:-21s% %memory:21s%\n"
        );
        $progressBar->setBarCharacter('<fg=green>⚬</>');
        $progressBar->setEmptyBarCharacter("<fg=red>⚬</>");
        $progressBar->setProgressCharacter("<fg=green>➤</>");
        $progressBar->setRedrawFrequency(10);

        // Start progress
        $progressBar->start();

        do {
            [$batchCount, $offsetLine] = $this->runOneBatch(
                $input->getArgument('filename'),
                $progressBar,
                $batchSize,
                $offsetLine
            );
        } while ($batchCount === $batchSize);

        // Executes all updates.
        $this->logRepository->flush();

        // Finish progress bar
        $progressBar->setMessage("Uff, done :)", 'status');
        $progressBar->finish();

        $io->newLine();
        $io->success('Successfully finished importing all entries from filename.');

        // Unlock the process
        $this->release();

        return Command::SUCCESS;
    }

    /**
     * @return void
     * @throws DbalException
     */
    private function cleanAllAnalyticsEntries(): void
    {
        $qb = $this->logRepository->createQueryBuilder('l');

        try {
            // Delete all entries
            $qb->delete();
            $qb->getQuery()->getSingleScalarResult();

            // Alter table auto_increment
            $cmd = $this->entityManager->getClassMetadata(AnalyticsLog::class);
            $connection = $this->entityManager->getConnection();
            $connection->executeStatement('ALTER TABLE ' . $cmd->getTableName() . ' AUTO_INCREMENT = 1;');
        } catch (NoResultException|NonUniqueResultException $e) {
            return;
        }
    }

    private function runOneBatch(string $file, ProgressBar $progressBar, int $batchSize, int $nextLineOffset): array
    {
        $file = new SplFileObject($file);
        $file->setFlags(
            SplFileObject::SKIP_EMPTY |
            SplFileObject::DROP_NEW_LINE |
            SplFileObject::READ_AHEAD
        );

        $fileIterator = new LimitIterator($file, $nextLineOffset, $batchSize);
        $batchData = new ArrayCollection(iterator_to_array($fileIterator));
        $batchCount = $batchData->count();

        if ($batchCount > 0) {
            $batchDataKeys = $batchData->getKeys();
            $nextLineOffset = end($batchDataKeys) + 1;

            $progressBar->setMessage(
                sprintf(
                    'Import batch started from line #%s',
                    $nextLineOffset
                ),
                'status'
            );

            foreach ($batchData as $key => $line) {
                try {
                    $this->processBatchEntry($key, $line);
                } catch (Exception $e) {
                    // Once the exception is handled, it is no longer needed
                    // so we can mark it as useless and garbage-collectable
                    $e = null;
                    unset($e);
                }

                // Advance the progress
                $progressBar->advance();
            }

            // Once the batch of entries is updated, we don't need it anymore
            // so we mark it as garbage-collectable
            $batchData = null;
            unset($batchData);

            // Flushing and then clearing Doctrine's entity manager allows
            // for more memory to be released by PHP
            $this->logRepository->flush();
            $this->entityManager->clear();

            // Just in case PHP would choose not to run garbage collection,
            // we run it manually at the end of each batch so that memory is
            // regularly released
            gc_collect_cycles();
        }

        return [$batchCount, $nextLineOffset];
    }

    /**
     * @param int $lineIndex
     * @param string|null $lineData
     * @return void
     * @throws Exception
     */
    private function processBatchEntry(int $lineIndex = 0, string $lineData = null): void
    {
        // Extract and prepare data to import into database.
        preg_match('/^(\S*).*\[(.*)\]\s"([^"]*)"\s(\S*)$/', $lineData, $matches);

        // Skip this line in case it has another format.
        if (empty($matches)) {
            throw new Exception('Wrong line format.');
        }

        // Persist a new object
        $this->logRepository->add((new AnalyticsLog())
            ->setLine($lineIndex)
            ->setLoggingDate(new DateTime($matches[2]))
            ->setRequest($matches[3])
            ->setServiceName($matches[1])
            ->setStatusCode($matches[4])
        );
    }
}
