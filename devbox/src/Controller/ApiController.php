<?php

namespace App\Controller;

use App\Service\AnalyticsLogService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api', name: 'app_api_')]
class ApiController extends AbstractController
{
    #[Route('/count', name: 'count')]
    public function count(AnalyticsLogService $analyticsService, RequestStack $requestStack): JsonResponse
    {
        return $this->json([
            'counter' => $analyticsService->filterAndCountAnalyticsLogs($requestStack->getCurrentRequest()),
        ]);
    }
}
