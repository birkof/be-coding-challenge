<?php

namespace App\Service;

use App\Repository\LogRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class AnalyticsLogService
{
    public function __construct(private LogRepository $logRepository)
    {
    }

    /**
     * @param Request $request
     * @return int
     * @throws Exception
     */
    public function filterAndCountAnalyticsLogs(Request $request): int
    {
        // Filters mapping
        $filters = [
            'serviceNames' => $request->get('serviceNames'),
            'statusCode' => $request->get('statusCode'),
            'startDate' => $request->get('startDate'),
            'endDate' => $request->get('endDate'),
        ];

        try {
            return $this->logRepository->filterAndCountLogs($filters);
        } catch (Exception $exception) {
            // Log something about it...

            return 0;
        }
    }
}
