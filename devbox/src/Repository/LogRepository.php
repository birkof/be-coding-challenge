<?php

namespace App\Repository;

use App\Entity\AnalyticsLog;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

/**
 * @extends ServiceEntityRepository<AnalyticsLog>
 *
 * @method AnalyticsLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnalyticsLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnalyticsLog[]    findAll()
 * @method AnalyticsLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogRepository extends AbstractRepository
{
    public function __construct(ManagerRegistry $registry, private readonly TagAwareCacheInterface $analyticsLogCache)
    {
        parent::__construct($registry, AnalyticsLog::class);
    }

    public function add(AnalyticsLog $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AnalyticsLog $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Finding the newest inserted entry.
     * @return AnalyticsLog|null
     * @throws NonUniqueResultException
     */
    public function findLastInsertedRow(): ?AnalyticsLog
    {
        return $this
            ->createQueryBuilder('l')
            ->orderBy('l.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param array $filters serviceNames[]/startDate/endDate/statusCode
     * @param int $cacheTtl
     * @return int
     * @throws Exception
     */
    public function filterAndCountLogs(array $filters = [], int $cacheTtl = 3600): int
    {
        if (empty($filters)) {
            return 0;
        }

        $cacheKey = 'analytics';
        $qb = $this->createQueryBuilder('l');

        // Count found entries.
        $qb->select('COUNT(l.id) as count');

        // Filtering data by status code
        if (!empty($filters['statusCode'])) {
            $cacheKey .= '_status_code_' . $filters['statusCode'];
            $qb
                ->andWhere('l.statusCode = :statusCode')
                ->setParameter(
                    'statusCode',
                    (int)$filters['statusCode']
                );
        }

        // Filtering data by start date
        if (!empty($filters['startDate'])) {
            $cacheKey .= '_start_' . strtotime($filters['startDate']);
            $qb
                ->andWhere('l.loggingDate >= :startDate')
                ->setParameter(
                    'startDate',
                    new DateTime($filters['startDate'])
                );
        }

        // Filtering data end date
        if (!empty($filters['endDate'])) {
            $cacheKey .= '_end_' . strtotime($filters['startDate']);
            $qb
                ->andWhere('l.loggingDate <= :endDate')
                ->setParameter(
                    'endDate',
                    new DateTime($filters['endDate'])
                );
        }

        // Filtering data by service name[s]
        if (!empty($filters['serviceNames'])) {
            $cacheKey .= '_services_' . implode('_', $filters['serviceNames']);
            $qb
                ->andWhere('l.serviceName IN (:serviceNames)')
                ->setParameter(
                    'serviceNames',
                    $filters['serviceNames'],
                    Connection::PARAM_STR_ARRAY
                );
        }

        try {
            return $this->analyticsLogCache->get($cacheKey, function (ItemInterface $item) use ($cacheTtl, $qb) {
                $item->expiresAfter($cacheTtl);

                return $qb->getQuery()->getSingleScalarResult();
            });
        } catch (NoResultException|NonUniqueResultException|InvalidArgumentException $e) {
            // Maybe we should log something here.
            return 0;
        }
    }
}
