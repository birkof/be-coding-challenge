<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

abstract class AbstractRepository extends ServiceEntityRepository
{
    /**
     * @param $entity
     *
     * @return $this
     */
    public function persist($entity)
    {
        $this->getEntityManager()->persist($entity);

        return $this;
    }

    /**
     * @return $this
     */
    public function flush()
    {
        $this->getEntityManager()->flush();

        return $this;
    }


    /**
     * @param $entity
     *
     * @return $this
     */
    public function refresh($entity)
    {
        $this->getEntityManager()->refresh($entity);

        return $this;
    }
}
