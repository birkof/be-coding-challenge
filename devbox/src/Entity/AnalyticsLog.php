<?php

namespace App\Entity;

use App\Repository\LogRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Integer;

#[ORM\Entity(repositoryClass: LogRepository::class)]
#[ORM\Index(fields: ['serviceName'])]
#[ORM\Index(fields: ['loggingDate'])]
#[ORM\Index(fields: ['statusCode'])]
class AnalyticsLog
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'bigint')]
    private int $line;

    #[ORM\Column(type: 'integer')]
    private int $statusCode;

    #[ORM\Column(type: 'datetime')]
    private DateTimeInterface $loggingDate;

    #[ORM\Column(type: 'string', length: 128)]
    private string $serviceName;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $request;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLine(): int
    {
        return $this->line;
    }

    public function setLine(int $line): self
    {
        $this->line = $line;

        return $this;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function setStatusCode(int $statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function getLoggingDate(): DateTimeInterface
    {
        return $this->loggingDate;
    }

    public function setLoggingDate(DateTimeInterface $loggingDate): self
    {
        $this->loggingDate = $loggingDate;

        return $this;
    }

    public function getServiceName(): ?string
    {
        return $this->serviceName;
    }

    public function setServiceName(string $serviceName): self
    {
        $this->serviceName = $serviceName;

        return $this;
    }

    public function getRequest(): ?string
    {
        return $this->request;
    }

    public function setRequest(?string $request): self
    {
        $this->request = $request;

        return $this;
    }
}
