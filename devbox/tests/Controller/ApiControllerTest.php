<?php

declare(strict_types=1);

namespace Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiControllerTest extends WebTestCase
{
    private const API_COUNTER_ENDPOINT = '/api/count';

    public function testApiCountEndpoint(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, self::API_COUNTER_ENDPOINT);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertResponseFormatSame('json');
    }

    public function testApiCountEndpointResponse(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, self::API_COUNTER_ENDPOINT);

        $this->assertArrayHasKey('counter', json_decode($client->getResponse()->getContent(), true));
    }
}
